<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#00953B" />
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?> <?php bloginfo('description'); ?></title>
    <!-- AGREGA TUS ESTILOS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php bloginfo('template_url') ?>/assets/css/main.css" rel="stylesheet" type="text/css" />
    <link href="<?php bloginfo('template_url') ?>/assets/css/owl.theme.default.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Barlow:100,200,300,400,500,600" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?php bloginfo('template_url') ?>/assets/images/favicon.png"/>
    <!-- AGREGA TUS SCRIPTS -->
    <script src="<?php bloginfo('template_url') ?>/assets/js/jquery.min.js"></script>
    <script src="<?php bloginfo('template_url') ?>/assets/js/my-functions.js"></script>
    <script src="<?php bloginfo('template_url') ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url') ?>/assets/js/owl.carousel.min.js"></script>
    <script src="<?php bloginfo('template_url') ?>/assets/js/wow.min.js"></script>
    <?php
    wp_enqueue_script('jquery');
    wp_head();
    ?>
</head>
<body <?php body_class(); ?>>
<header>
    <nav class="navbar navbar-absolute">
        <aside class="new-nav">
            <input type="checkbox" id="menu__trigger">
            <section class="menu">
                <!--<label for="menu__trigger" class="entypo-menu"><i class="Flmenu"></i> MENÚ</label>-->
                <nav>
                    <ul>
                        <li><a href="#">1. Menu Item</a></li>
                        <li>
                            <a href="#">2. Menu Item</a>
                            <ul>
                                <li><a href="#">1. Sub Item</a></li>
                                <li><a href="#">2. Sub Item</a></li>
                                <li><a href="#">3. Sub Item</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">3. Menu Item</a>
                            <ul>
                                <li><a href="#">1. Sub Item</a></li>
                                <li><a href="#">2. Sub Item</a></li>
                                <li><a href="#">3. Sub Item</a></li>
                            </ul>
                        </li>
                        <li><a href="#">4. Menu Item</a></li>
                        <li><a href="#">5. Menu Item</a></li>
                        <li><a href="#">6. Menu Item</a></li>
                        <li><a href="#">7. Menu Item</a></li>
                        <li><a href="#">8. Menu Item</a></li>
                    </ul>
                </nav>
            </section>

            <section class="content">
                <label for="menu__trigger" class="entypo-menu"><i class="Flmenu"></i> MENÚ</label>
                <div class="search">
                    <form role="search" method="get" id="form" action="/">
                        <input class="search-box" type="search" name="s" placeholder="Buscar">
                    </form>
                    <span class="search-button">
                        <span class="search-icon"></span>
                    </span>
                </div>
                <nav class="rrss-nav">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </nav>
            </section>
        </aside>
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="/">
                    <img src="<?php bloginfo('template_url') ?>/assets/images/logo-fenalco.svg" alt="Logo Fenalco">
                </a>
            </div>
        </div><!-- /.container-fluid -->
    </nav>
</header>