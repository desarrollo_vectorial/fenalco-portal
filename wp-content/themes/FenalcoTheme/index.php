<?php

get_header();
wp_head();

?>
    <div class="container-fluid" style="min-height: 60vh;">
        <div class="row">
            <div class="slider">
                <?php echo do_shortcode('[rev_slider alias="slider-home1"]'); ?>
            </div>
        </div>
        <div class="toltips-home">
            <ul>
                <li>
                    <a href="#GOOGLE">
                        <i class="Flicon-1" data-toggle="tooltip1" data-placement="right" title="Formación Empresarial">
                            <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span>
                        </i>
                    </a>
                </li>
                <li>
                    <a href="#GOOGLE">
                        <i class="Flicon-2" data-toggle="tooltip2" data-placement="right" title="Eventos y Ferias">
                            <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span>
                        </i>
                    </a>
                </li>
                <li>
                    <a href="#GOOGLE">
                        <i class="Flicon-3" data-toggle="tooltip3" data-placement="right" title="Servicios Financieros">
                            <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span>
                        </i>
                    </a>
                </li>
                <li>
                    <a href="#GOOGLE">
                        <i class="Flicon-4" data-toggle="tooltip4" data-placement="right" title="Servicios BPO">
                            <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span>
                        </i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid" id="services-grid">
        <div class="row">
            <div class="col-md-3">
                <div class="full-box wow fadeInLeftBig" style="background-image:url(/wp-content/uploads/2018/01/pagos-linea2.jpg); ">
                    <p>Servicio de<br><strong> pagos en línea</strong></p>
                    <span style="background: #F28800;"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="half-box wow fadeInRightBig" data-wow-duration="1s" data-wow-delay="1s" style="background-image:url(/wp-content/uploads/2018/01/formacion-empresarial.jpg); ">
                    <p>Formación<br><strong> Empresarial</strong></p>
                    <span style="background: #014F8D;"></span>
                </div>
                <div class="half-box wow fadeInRightBig" data-wow-duration="1s" data-wow-delay="1.6s" style="background-image: url(/wp-content/uploads/2018/01/gestion-gremial.jpg);">
                    <p>Gestión<br><strong> gremial</strong></p>
                    <span style="background: #00A5EF;"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="half-box wow fadeInRightBig" data-wow-duration="1s" data-wow-delay="1.2s" style="background-image: url(/wp-content/uploads/2018/01/eventos-ferias.jpg);">
                    <p>Eventos<br><strong> y ferias</strong></p>
                    <span style="background:  #00A5EF;"></span>
                </div>
                <div class="half-box wow fadeInRightBig" data-wow-duration="1s" data-wow-delay="1.8s" style="background-image: url(/wp-content/uploads/2018/01/afiliate-fenalco.jpg);">
                    <p>Afiliate a<br><strong> fenalco</strong></p>
                    <span style="background: #014F8D;"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="half-box wow fadeInRightBig" data-wow-duration="1s" data-wow-delay="1.4s" style="background-image: url(/wp-content/uploads/2018/01/servicios-financieros.jpg);">
                    <p>Servicios<br><strong> Financieros</strong></p>
                    <span style="background: #F28800;"></span>
                </div>
                <div class="half-box wow fadeInRightBig" data-wow-duration="1s" data-wow-delay="2s" style="background-image: url(/wp-content/uploads/2018/01/servicios-bpo.jpg);">
                    <p>Servicios<br><strong> BPO</strong></p>
                    <span style="background: #B1B801;"></span>
                </div>
            </div>
        </div>
    </div>
<div class="container-fluid" id="slider-news">
    <div class="row">
        <div class="col-md-8 wow rotateInUpLeft" data-wow-delay=".2s">
            <div class="slider-2">
                <?php echo do_shortcode('[rev_slider alias="slider-home2"]'); ?>
            </div>
        </div>
        <div class="col-md-4 wow rotateInUpRight" data-wow-delay=".2s">
            <div class="news">
                <p>Temas de interés <span></span></p>
                <ul>
                    <li>
                        <p>Ago 31, 2017</p>
                        <a href="#!">Ahora si el poder es de los consumidores</a>
                    </li>
                    <li>
                        <p>Ago 31, 2017</p>
                        <a href="#!">La evolución de las tarjetas de crédito: ¿comprar sin pasar por caja?</a>
                    </li>
                    <li>
                        <p>Ago 31, 2017</p>
                        <a href="#!">El negocio del retail es bien difícil</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" id="indicadores">
    <div class="row">
        <div class="col-md-2 indicadores-title">
            <p>Indicadores<br><span>económicos</span></p>
        </div>
        <div class="col-md-9">
            <?php get_template_part('indicadores'); ?>
        </div>
    </div>
</div>
<?php

wp_footer();
get_footer();

?>