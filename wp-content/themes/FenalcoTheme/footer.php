<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-to-5">
                <p><strong>Fenalco</strong></p>
                <ul>
                    <li><a href="#">Quiénes somos</a></li>
                    <li><a href="#">Afiliate a Fenalco</a></li>
                    <li><a href="#">Trabajo Sectorial</a></li>
                    <li><a href="#">Comunicaciones</a></li>
                    <li><a href="#">Trabaje con nosotros</a></li>
                    <li><a href="#">Publicaciones</a></li>
                    <li><a href="#">Política de Datos</a></li>
                    <li><a href="#">Contáctenos</a></li>
                    <li><a href="#">PQRS</a></li>
                </ul>
            </div>
            <div class="col-md-to-5">
                <p><strong>Formación empresarial</strong></p>
                <ul>
                    <li><a href="#">Nuestra escencia</a></li>
                    <li><a href="#">Nuestros servicios</a></li>
                    <li><a href="#">Tertimoniales</a></li>
                    <li><a href="#">Agenda de formación</a></li>
                </ul>
            </div>
            <div class="col-md-to-5">
                <p><strong>Servicios financieros</strong></p>
                <ul>
                    <li><a href="#">Servicio de Aval</a></li>
                    <li><a href="#">Servicios financieros</a></li>
                </ul>
            </div>
            <div class="col-md-to-5">
                <p><strong>Servicios de BPO</strong></p>
                <ul>
                    <li><a href="#">Fenalcontact</a></li>
                    <li><a href="#">Fenalcobranzas</a></li>
                </ul>
            </div>
            <div class="col-md-to-5">
                <p><strong>Eventos y Ferias Comerciales</strong></p>
                <ul>
                    <li><a href="#">Nuestra escencia</a></li>
                    <li><a href="#">Expopyme</a></li>
                    <li><a href="#">Construhogar</a></li>
                    <li><a href="#">Expoindustrial</a></li>
                    <li><a href="#">Expomotor</a></li>
                    <li><a href="#">Día Nacional del Tendero</a></li>
                    <li><a href="#">Cali Exposhow</a></li>
                </ul>
            </div>
        </div>
        <div class="row" id="copy">
            <div class="col-md-12">
                <p><strong>Copyright todos los derechos reservados - Fenalco 2017</strong></p>
                <span>Powered by: <a href="http://www.vectorial.co" target="_blank">Vectorial</a></span>
            </div>
        </div>
    </div>
</footer>

        <?php wp_footer(); ?>
        </body>
        </html>