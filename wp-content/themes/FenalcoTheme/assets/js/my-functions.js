jQuery(function ($) {

    //TOOLTIP
    $(function () {
        $('[data-toggle="tooltip1"]').tooltip();
        $('[data-toggle="tooltip2"]').tooltip();
        $('[data-toggle="tooltip3"]').tooltip();
        $('[data-toggle="tooltip4"]').tooltip();
    })

    //SEARCH
    $('.search-button').click(function(){
        $(this).parent().toggleClass('open');
    });

    //WOW
    var wow = new WOW();
    wow.init();

    //OWL
    $(".owl-carousel").owlCarousel({
        items: 6,
        autoWidth:true,
        //margin:10,
        dots:false,
        nav:true,
        loop:true,
        mouseDrag:false,
        navText: ["<span>&lt;</span>", "<span><i class='Flline'><span class='path1'></span><span class='path2'></span></i></span>"],
        responsive:{
            0:{
                items:1
            }
        }
    });

});