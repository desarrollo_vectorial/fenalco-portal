<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Indicadores</title>
</head>
<body>
<?php

// Definimos la función cURL
function curl($url) {
    $ch = curl_init($url); // Inicia sesión cURL
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // Configura cURL para devolver el resultado como cadena
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Configura cURL para que no verifique el peer del certificado dado que nuestra URL utiliza el protocolo HTTPS
    $info = curl_exec($ch); // Establece una sesión cURL y asigna la información a la variable $info
    curl_close($ch); // Cierra sesión cURL
    return $info; // Devuelve la información de la función
}

$html = curl("https://widgetsdataifx.blob.core.windows.net/semana/semanaindicators");  // Ejecuta la función curl escrapeando el sitio web https://devcode.la and regresa el valor a la variable $sitioweb
//var_dump(strlen($data));
$dom = new DOMDocument();
@$dom->loadHTML($html);
$xpath = new DomXpath($dom);
$entries = $xpath->query("//table[@class='content']/tbody/tr[@class='item-row']");

$results = array();
foreach ($entries as $entry) {
    // pass in the $entry node as the context node, the the query is relative to it

    /*TRAEMOS EL DOLAR*/
    $node = $xpath->query("td[@class='item-name']", $entry); // returns a DOMNodeList
    $result['nombre'] = $node->item(0)->nodeValue;

    $node = $xpath->query("td[@class='item-value']/span", $entry);
    $result['precio'] = $node->item(0)->nodeValue;

    $node = $xpath->query("td[@class='item-value']/span/attribute::class", $entry);
    $result['clase'] = $node->item(0)->value;


    $results[] = $result;
}
/*print_r($results);*/
echo '<ul id="table-indicadores" class="owl-carousel">';
//echo '<tbody>';
$count = 1;

foreach($results as $valor) {
    if($count == 0 and $count <= 2){
        echo '<tr>';
    }

    echo '<li class="item ' . $valor['clase'] . '"><p>' . $valor['nombre'] . '</p>' .  '<span>' . $valor['precio'] . '</span>' . '</li>';

    if($count == 3){
        $count = 0;
        echo '</tr>';
    }
    $count++;
}

//echo '</tbody>';
echo '</ul>';


?>
</body>
</html>