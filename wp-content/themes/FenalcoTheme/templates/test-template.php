<?php

/* Template Name: TEST TEMPLATE */

get_header();
wp_head();

?>
    <h1><?php echo get_the_title() ?></h1>
<?php
if (have_posts()) {
    while (have_posts()) {
        the_post();
        echo the_content();
    }
}
?>
<?php

wp_footer();
get_footer();

?>