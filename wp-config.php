<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

define('HTTP_HOST', $_SERVER["HTTP_HOST"]);
define('LOCAL_DOMAIN', 'fenalco.portal.local');
define('DEV_LOCAL_VIEW', 'fenalco.192.168.0.40.xip.io');
define('DEV_DOMAIN', 'fenalco-portal.vectorial.co');
define('PROD_DOMAIN', '');
define('PROD_DOMAIN2', '');



// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //

switch (HTTP_HOST) {
    case LOCAL_DOMAIN:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://fenalco.portal.local/');
        define('WP_SITEURL','http://fenalco.portal.local/');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'dev_fenalco_portal');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;
    case DEV_DOMAIN:
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'fenalco_portal');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'vectorial$_$');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', '127.0.0.1');
        break;

    case DEV_LOCAL_VIEW:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://fenalco.192.168.0.40.xip.io');
        define('WP_SITEURL','http://fenalco.192.168.0.40.xip.io');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'dev_fenalco_portal');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;
    case PROD_DOMAIN:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','');
        define('WP_SITEURL','');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'hematoon_portal');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'hematoon_user_portal');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'xv]u86hJ2W)G');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;

    case PROD_DOMAIN2:
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'webcrc_w3bs1t3');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'webcrc_w3bs1t3');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'crcv4ll3w3bs1t3');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', '127.0.0.1');
        break;
    default:
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', '');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', '');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', '');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', '');
        break;
}

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '? n?K#G<O_yn# pgdxP8Z<({m6LNBHe|W:`JmO_P/iwy>X%P[2dJZk>_)2B]:4CN');
define('SECURE_AUTH_KEY', 'W|,<+bTa?eWa}|n`A4XyI[[!#H5ezx$6;N7lW6pE|6O7N`n.W0iQ)D#5;#83N)e]');
define('LOGGED_IN_KEY', '|f<Ud@?BaPs`XC(*HqE=zulO6kuqPcJt`oED!s.T?dCR%Nv3?URy50~9@,&g&(,Y');
define('NONCE_KEY', '(@de[6!=F~00I-8E|;,Bq;*+xXLP:9{Ff@sMT;$5trWI B)J?sj@o?aTPB~u jW)');
define('AUTH_SALT', '3)Dw(0=06vI=q7Tk0MX;ujal[jY%Z`F}J@ZbXo4HpcJO A]Q.vyrPeOdcX:jwc$#');
define('SECURE_AUTH_SALT', 'egvyin-4IYYjJdy01uUKtv=*hB|pWT@A;n[|1sE!CaOPp%*{u7fXGuMN =YVO-f2');
define('LOGGED_IN_SALT', '(1I8.vwN&{GNL/GDH{pFG ,~_O342g}D:gnKz]+jDLBj~4s(:2ndVlo_0C1Gaj8.');
define('NONCE_SALT', 'a[5_ekdXHfp`{mLf{dK0`>BB2L]d;g|WEy f/bU<bHAR}H0n,kDUhY!wJ0aX%s^t');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'fl_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

